# Architecture-as-Code

This fork of Structurizr bootstraps an Architecture-as-Code repository.

## Features

### [C4 Model](https://c4model.com/) Notation Template

![C4](assets/c4.png)

### [Arc42](https://arc42.org) Documentation Template

![arc42](assets/arc42.png)

### Architectural Decision Records Template

![ADR](assets/adr.png)

### SVG Preview

`/workspace/preview/{diagram}`

### Iframe Embedding

`/workspace/embed/{diagram}`

## Usage

- Copy the `workspace` directory.
- Edit models and views in `workspace.dsl`.
- Edit documentation in `docs`.
- Edit architecture decision records in `adrs`.
- Push to your private Git repository.
- Deploy in the intranet of your company.
- Embed the views within Confluence.
- Print the documentation.

Read-only publishing:
```shell
cd workspace
docker run -it --rm -p 8080:8080 -v $(pwd):/workspace -e "EDITABLE=false" registry.gitlab.com/plgrnd/aac:2.1.7
```

## Development

- Requires JDK 17 or higher.

## Credits

- Simon Brown, [Structurizr Lite](https://github.com/structurizr/lite)
