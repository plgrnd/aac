# Template

Date: 2024-01-01

## Status

Draft

## Decision

Describe here our response to these forces, that is, the design decision that was made. State the decision in full sentences, with active voice.

## Rationale

Describe here the rationale for the design decision. Also indicate the rationale for significant *rejected* alternatives. This section may also indicate assumptions, constraints, requirements, and results of evaluations and experiments.

## Consequences

Describe here the resulting context, after applying the decision. All consequences should be listed, not just the "positive" ones. 
