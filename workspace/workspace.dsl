workspace "System Name" {
    !identifiers hierarchical
    !adrs adrs
    !docs docs
    model {
        user = person "User" "" ext
        system = softwareSystem "System" {
            frontend = container "Frontend" "HTTP 80" {
                technology "Node.js"
                app = component "NodeJS Application" "HTTP 3000" {
                    technology "React"
                }
            }
            backend = container "Backend" "HTTP 8000" {
                technology "Java"
                app = component "Java Application" "HTTP 8000" {
                    technology "Spring Boot"
                    frontend.app -> this "HTTP" "REST"
                }
            }
            database = container "Database" "TCP 3306" {
                tags db
                technology "RDBMS"
                app = component "MySQL" "Relational Database" "TCP 3306" {
                    technology "C++"
                    backend.app -> this "TCP" "SQL"
                }
            }
            cache = container "Cache" "TCP 6379" {
                tags db,planned
                technology "In-memory Cache"
                app = component "Redis" "Key-value Database" "TCP 6379" {
                    technology "C"
                    backend.app -> this "TCP" "RESP" planned
                }
            }
        }
        user -> system.frontend.app "L7 Protocol" "MIME Type"

        production = deploymentEnvironment "Production" {
            deploymentNode "PaaS" {
                deploymentNode "Availability Zone" {
                    frontend = deploymentNode "Frontend" frontend {
                        containerInstance system.frontend
                        instances 3
                    }
                    backend = deploymentNode "Backend" {
                        containerInstance system.backend
                        instances 3
                    }
                    database = deploymentNode "Database" {
                        containerInstance system.database
                    }
                    cache = deploymentNode "Cache" {
                        containerInstance system.cache
                    }
                    lb = infrastructureNode "Load Balancer"
                    lb -> frontend "HTTP" "HTML, CSS, JS, WEBP"
                }
            }
        }
    }
    views {
        systemContext system context {
            title "Context Diagram"
            include *
            autolayout
        }
        container system containers {
            title "Container Diagram"
            include *
            autolayout
        }
        component system.frontend frontend {
            title "Component Frontend"
            include *
            autolayout
        }
        component system.backend backend {
            title "Component Backend"
            include *
            autolayout
        }
        deployment system production production {
            title "Deplyment Production"
            include *
            autoLayout
        }
        styles {
            theme default
            element ext {
                background grey
            }
            element db {
                shape Cylinder
            }
            element planned {
                strokeWidth 10
                opacity 50
                stroke green
            }
            relationship planned {
                color green
            }
        }
    }
}