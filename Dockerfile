FROM maven:3.8-openjdk-17 AS build
WORKDIR /workspace/app
COPY pom.xml .
RUN mvn -B -e -C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline
COPY . .
RUN mvn clean package

FROM eclipse-temurin:17-jre
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends graphviz
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build /workspace/app/target/aac.war app.war
RUN groupadd -r app && useradd --no-log-init -r -g app app
RUN chown app:app app.war
USER app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.war"]
